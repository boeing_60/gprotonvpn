# main.py
#
# Copyright 2020 Clément
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi
import subprocess


gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio

from .window import GprotonvpnWindow


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id='fr.jetestundomaine.gprotonvpn',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        win = self.props.active_window #at the beginning it's not active so...
        if not win:
            win = GprotonvpnWindow(application=self)
        win.label2.set_text("it works ghfghfgfghfgh!")
        win.header.set_subtitle("lol")
        #p = subprocess.run(["flatpak-spawn","--host","pkexec","apt","update"])
        #p = subprocess.run(["flatpak-spawn","--host","pkexec","protonvpn","init"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, encoding='utf-8')
        p = subprocess.Popen(["flatpak-spawn --host pkexec protonvpn init"], shell=True,stdin=subprocess.PIPE, stdout=subprocess.PIPE, encoding='utf-8')
        print(p.stdout.readline())
        p.stdin.write('Y\n')#oui override
        p.stdin.flush()
        p.stdin.write('lol\n') #pseudo#tant pis pour les readlines, les writes eux fonctionne à meirveilles
        p.stdin.flush()
        p.stdin.write('test\n')#mdp
        p.stdin.flush()
        p.stdin.write('test\n')#mdp conf
        p.stdin.flush()
        p.stdin.write('1\n')#PLAN
        p.stdin.flush()
        p.stdin.write('2\n')#UDP or TCP
        p.stdin.flush()
        p.stdin.write('Y\n') #confirmation
        p.stdin.flush()
        win.present()



def main(version):
    app = Application()
    return app.run(sys.argv)
